package com.example.businesscardapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.businesscardapp.ui.theme.BusinessCardAppTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BusinessCardAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    BusinessCard()
                }
            }
        }
    }
}

@Composable
fun GreetingSection(modifier: Modifier = Modifier) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
        Image(painter = painterResource(id = R.drawable.android_logo), contentDescription = "Android logo pic", modifier = Modifier
            .size(100.dp)
            .background(color = Color(7,48, 66))
            .padding(0.dp))
        Text(text = "Cheenmaya Bhutad", fontSize = 35.sp)
        Text(text = "Android Developer", color = Color(30, 127, 80))
    }

}

@Composable
fun ContactSection(modifier: Modifier = Modifier) {
    Column(modifier = modifier,
        verticalArrangement = Arrangement.Center) {
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
            Icon(painter = painterResource(id = R.drawable.ic_baseline_local_phone_24), contentDescription = "phone icon", tint = Color(0,109,59), modifier = Modifier
                .weight(1f)
                .padding(start = 60.dp))
            Text(text = "+91-8554888660", modifier = Modifier.weight(2f))
        }
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
            Icon(painter = painterResource(id = R.drawable.ic_baseline_share_24), contentDescription = "social share icon", tint = Color(0,109,59), modifier = Modifier
                .weight(1f)
                .padding(start = 60.dp))
            Text(text = "@cbhutad", modifier = Modifier.weight(2f))
        }
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
            Icon(painter = painterResource(id = R.drawable.ic_baseling_email_24), contentDescription = "social share icon", tint = Color(0,109,59), modifier = Modifier
                .weight(1f)
                .padding(start = 60.dp))
            Text(text = "testmailcbhutad@gmail.com", modifier = Modifier.weight(2f))
        }
    }
}

@Composable
fun BusinessCard() {
    Column(Modifier.fillMaxWidth().fillMaxHeight().background(color = Color(210,232,212))) {
        GreetingSection(modifier = Modifier.weight(3f).fillMaxWidth())
        ContactSection(modifier = Modifier.weight(1f))
    }
}


@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    BusinessCardAppTheme {
        //GreetingSection()
        //ContactSection()
        BusinessCard()
    }
}